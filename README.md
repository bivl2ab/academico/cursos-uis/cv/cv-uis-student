# Visión por computador  2019-2

<img src="/imgs/logo_CV.jpeg" style="width:400px;">


_Regístrate [aquí](https://forms.gle/U8FStrd7y99eyrf59)
                                            
La máquina virtual puede descargarse [aquí](https://drive.google.com/file/d/1KxCUZlXDgyvJzfs6s7EfegMVS1HL_bXq/view?usp=sharing)


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google....by free! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar tu recursos de computador Local. 

## Máquina Virtual

Alternativamente, usaremos esta máquina virtual que tiene instalado un entorno Python (2) Anaconda con Jupyter Notebooks disponibles en  [localhost:8008/tree](http://localhost:8008/tree) una vez que la máquina arranca.

- Esta opción es util para contestar talleres y para trabajar desde casa. 
- También para contestar parciales desde la U.

**Observa la configuración de la máquina**

- Si tu máquina física tiene al menos 4GB de memoria configura la máquina virtual **con 2GB de memoria**
- Aunque casi no necesitarás un terminal, el interfaz de Jupyter Notebooks tiene un terminal para acceder a través del navegador. En cualquier caso, la máquina virtual tiene un servidor SSH en el puerto 2222 con user/user como usuario y pwd. Si tu máquina física es mac o linux usa `ssh -p 2222 user@localhost` para conectarte. Si es windows, usa [putty](https://www.putty.org/)
- Si compartes una carpeta entre la física y virtual asegúrate que **el nombre con el que se comparte** sea `share` (aunque el nombre de la carpeta en la máquina física puede ser distinto)

**Para montar la carpeta compartida** ejecuta lo siguiente en un terminal y la carpeta aparecerá en /home/user/share:

    sudo mount share


## Calificación
- 20% Talleres primer corte (Problemsets)
- 20% Talleres segundo corte (Problemsets)
- 20% Talleres tercer corte (Problemsets)
- 40 % + [10% ,20 %, 30 %] Proyecto funcional IA 
    - 10% -> Hasta + una unidad en una nota de un 10% (los parciales por ejemplo) o su equivalente en otros porcentajes. 
    - Las notas o porcentajes adicionales se obtienen si fueron puntuales en todas las entregas. 



## Talleres (Problemsets)

Los talleres pretender ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres ser desarrollará en casa, dentro de las fechas establecidas en el cronograma. 

## Parciales (Quizes)

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Los apuntes y notebooks del curso se pueden utilizar. 

## Proyecto funcional CV

- **Funcionamiento del proyecto**. El proyecto se debe realizar como un notebook.  

- **Prototipo [+10% - +20%]**:  En este item se considera como esta estructurado el proyecto. Los porcentajes extras tienen en cuenta importancia o relevancia de 
del proyecto y también la solución a problemas  reales.

- **Presentación (banner, video y diapositivas) [+ 10%]**:  Se debe enviar un video corto (max 5 minutos) y un documento de máximo 5 páginas en donde se exponga: 

1- La motivación para el desarrollo del proyecto
2- El tema principal de inteligencia artificial abordado
3- funcionamiento y simulación del proyecto


- **Preguntas**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
Todos los items tienen el mismo porcentaje de evaluación. 

- **PRE-SUS PROJ**: En la presustentación del proyecto se deben presentar avances y se dará un estimado de la nota definitiva. En las siguientes semanas se tendrá chance de mejorar la nota según previas observaciones. 

## Calendario 


                       SESSION 1            SESSION 2           STUDENT DEADLINES

     W01 Sep16-Sep20    Intro CV            1.Intro tools             
     W02 Sep23-Sep27    ----		    -----            	U18-participate in one challenge
     W03 Sep30-Oct04    2.Img Class.	    3.Histograms        
     W04 Oct07-Oct11    4. Spatial proc		
     W05 Oct18-Oct14    4.Back Sub          4. Opt Flow         Festivo 14 Octubre 
     W06 Oct21-Oct25    5.Opt Flow          5. PSETS            Oct26 Registro primera calificación
     
     ---                ---                ---                   ---
     little break       ---                ----                  ---
     ---                ---                ---                   ---  
     W07 Jan14-Jan15    ---                REVIEW                ---
     W08 Jan21-Jan22    CACEROLAZO         REVIEW                ---            
     
     W09 Jan28-Jan29    6.KeyPoint          7.ORB point          --- 
     W10 Feb04-Feb05    8.BoW               8.BoW                Entrega segundo PSET   
     W11 Feb11-Feb12    9. Grad Des         10.DNN  	         ---
     W12 Feb18-Feb19    11.CNN              12.CNN Architectures ---     
     W13 Feb25-Feb26   PSETS               PSETS 		         ---
     W14 Mar03-Mar04    13.deep Features    PRE-SUS PROJ         ---
     W15 Mar10-Mar11    14.deep Features    0.dropout&transfer   ---
     W16 Mar17-Mar18    13.deep Features    14. transferL        virtual 
     W17 Mar24-Mar25    15.dropout_other    Prep projec       	 virtual
	 W18 Mar21-Apr01    Prep project        Project          	 virtual
		                
		                



    Feb03 -        -> Registro primera calificación
    Feb09 -        -> Último día cancelación materias
    Mar27 -        -> Finalización clase
    Mar30-Abr03    -> Evaluaciones finales
    Abr03 -        -> Registro calificaciones finales
    
[Calendario academico](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2019/acuerdoAcad064_2019.pdf)

**CUALQUIER ENTREGA FUERA DE PLAZO SERÁ PENALIZADA CON UN 50%**
**LOS PROBLEMSETS ESTAN SUJETOS A CAMBIOS QUE SERÁN DEBIDAMENTE INFORMADOS%**
**DEADLINE DE LOS PROBLEMSETS SERÁN EL DIA DE CADA QUIZ**

