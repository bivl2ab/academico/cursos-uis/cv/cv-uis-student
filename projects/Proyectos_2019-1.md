---
# Proyectos 2019-1. Visión por Computador. 

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [Detección de pólipos para apoyar el diagnóstico del cáncer de colon](#proy1)
2. [Sistema automático para el reconocimiento de placas](#proy2)
3. [Predicción de patologías cardiacas con métodos convolucionales](#proy3)
4. [Siendo tus ojos](#proy4)
5. [Deteccion-de-caracteres-alfanumericos-en-placas-de-vehiculos-colombianos](#proy5)
6. [ArtSource](#proy6)
7. [GreenBox_prototipe](#proy7)
8. [Descriptor de Acciones mientras se Conduce](#proy8)
9. [Reconocimiento de actividades usando la covarianza local](#proy9)
10. [Cardiac Cycle generation](#proy10)
11. [Smart Dump](#proy11)
12. [Cardiac Cycle Generation](#proy12)
---

## Detección de pólipos para apoyar el diagnóstico del cáncer de colon <a name="proy1"></a>

**Autores: Lina Marcela Ruiz García**

<img src="imgs/banner_cancer.png" style="width:700px;">

**Objetivo:Implementar diferentes técnicas de machine learning y deep learning para la detección de pólipos, con el fin de apoyar las tareas de diagnóstico del cáncer de colón**  

- Dataset: 38 videos de colonoscopias reales (con y sin pólipos), 18702 frames para entrenamiento y 17574 frames para test
- Modelo: Modelo basado en ORB, modelo CNN y transfer learning

[(code)](https://gitlab.com/linamruiz/proyectovision); [(video)](https://drive.google.com/file/d/1CwdWZWAhWxy83FEZ_fB2CnoUaezrmh40/view?usp=sharing); [(+info)](https://drive.google.com/file/d/1yw3lrEZ40pOf_k5JOIts6nOr-aOejFyD/view?usp=sharing)

---
## Sistema automático para el reconocimiento de placas <a name="proy2"></a>

**Autores: Jorge Saul Castillo Jaimes**

<img src="imgs/detplacas.jpg" style="width:700px;">

**Objetivo:Crear un sistema que permita el reconocimiento de placas de los vehículos que ingresan a los parqueaderos de la Universidad Industrial de Santander**  

- Dataset:  Un dataset con 74 mil imagenes de caracteres y un dataset elaborado utilizando las normas colombianas para las placas
- Modelo: ALPR (Captura de la imagen, RoI, Transformación de la imagen, Reconocimiento de caracteres)

[(code)](https://github.com/hopkeinst/2019-1_CVProject); [(video)](https://www.youtube.com/watch?v=cPiWHs5UfCQ);

---
## Predicción de patologías cardiacas con métodos convolucionales <a name="proy3"></a>

**Autores: Alejandra Moreno**

<img src="https://github.com/AlejandraM97/VISION-CODE/blob/master/BANNER_VISION1.jpg" style="width:700px;">

**Objetivo: clasificar las enfermedades cardiovasculares y así brindarle una herramienta al doctor para determinar si un paciente tiene una patología cardiaca en específica.**  

- Dataset: SunnyBrook" propuesto en el challenge de MICCAI 2009. Consta de 45 pacientes, 
- Modelo: arquitectura 3D convolucional

[(code)](https://github.com/AlejandraM97/VISION-CODE); [(video)](https://drive.google.com/file/d/16AJCkNDserTH828EcUNmPHl6IBSmEfG1/view?ts=5d5cdc94); [(+info)](https://github.com/AlejandraM97/VISION-CODE/blob/master/Proyecto_Vision.pdf)

---


## Siendo tus ojos <a name="proy4"></a>

**Autores: Verónica Lucena y Karina Sequeda**

<img src="https://github.com/sequedakarina/SIENDO-TUS-OJOS/blob/master/banner.png" style="width:700px;">

**Objetivo: Brindar una herramienta de apoyo a aquellas personas con discapacidad visual mediante el reconocimiento de caracteres traducidos a señales auditivas.**  

- Dataset:  creado por estudiantes que escriben letras con un total 832 imágenes etiquetada
- Modelo: Transfer Learning, CNN, DNN

[(code)](https://github.com/sequedakarina/SIENDO-TUS-OJOS); [(video)](https://github.com/sequedakarina/SIENDO-TUS-OJOS/blob/master/VideoProyectoCV.mp4); [(+info)](https://github.com/sequedakarina/SIENDO-TUS-OJOS/blob/master/PresentacionCV.pdf)




---
## Deteccion-de-caracteres-alafnumericos-en-placas-de-veiculos-colombianas <a name="proy5"></a>

**Autores: Jhon Edinson López Duran**

<img src="imgs/placas.jpg" style="width:700px;">

**Objetivo: Detección de la placa de un vehículo para su futura consulta en una base de datos para saber el estado legal del vehículo.**  

- Dataset: 74 imagenes de Chars74K
- Modelo: Arquitectura de red neuronal convolucional basada en Lenet, canny, BoundingRect, GaussianBlur.

[(code)](https://github.com/JhEdLp/Deteccion-de-caracteres-alfanumericos-en-placas-de-vehiculos-colombianos/tree/master/notebooks); [(video)](https://github.com/JhEdLp/Deteccion-de-caracteres-alfanumericos-en-placas-de-vehiculos-colombianos/blob/master/V%C3%ADdeos/video-vision-2019-08-21_12.02.16.mp4); [(+info)](https://github.com/JhEdLp/Deteccion-de-caracteres-alfanumericos-en-placas-de-vehiculos-colombianos/blob/master/Presentaci%C3%B3n/presentaci%C3%B3n-final.pdf)

---
---
## ArtSource <a name="proy6"></a>

**Autores: Juan Pablo Moreno, Maria fernanda navas, Juan David Niño**

<img src="https://github.com/fhum/ArtSource/blob/master/logo.png" style="width:700px;">

**Objetivo: ArtSource es un proyecto de inteligencia artificial que busca clasificar las pinturas de arte según su corriente artística.**  

- Dataset: 79000 imagenes de arte, 27 estilos artísticos 
- Modelo: BoW, CNN, transferLearning

[(code)](https://github.com/fhum/ArtSource); [(video)]( https://youtu.be/1LOi7YGIMLA); [(+info)](https://github.com/fhum/ArtSource/blob/master/presentation.pdf)

---
## GreenBox prototipe <a name="proy7"></a>

**Autores: Edgar Montenegro, Leyston Oñate, Franklin Sierra**

<img src="https://github.com/EdgarAndresMontenegro/GreenBox_prototipe/blob/master/img/banner.png" style="width:700px;">

**Objetivo:Diseño de un prototipo funcional que vision por computadora y dispositivos microcontroladores para la seguridad de un entorno restringiendo el acceso a personal no autorizado**  

- Dataset: el dataset FACE MATCHING DATA SET
- Modelo: CNN, BoW

[(code)](https://github.com/EdgarAndresMontenegro/GreenBox_prototipe); [(video)](https://github.com/EdgarAndresMontenegro/GreenBox_prototipe/blob/master/video_final.mp4); [(+info)](https://github.com/EdgarAndresMontenegro/GreenBox_prototipe/blob/master/Presentacion/Sistema%20de%20reconocimiento%20e%20identi%EF%AC%81cacion%20facial%20aplicado%20a%20la%20seguridad%20laboral.pdf)

---

## Descriptor de Acciones mientras se Conduce <a name="proy8"></a>

**Autores: Henry Peña, Diego Medina**

<img src="https://camo.githubusercontent.com/d285af647b6a686368d3ffed724cce165c6930e1/68747470733a2f2f692e696d6775722e636f6d2f5872706e75764d2e6a7067" style="width:700px;">

**Objetivo:Clasificar las Acciones de un conductor al Volante.**  

- Dataset: 22400 imágenes y 10 acciones al conducir
- Modelo: Deeplearning

[(code)](https://github.com/pecons/proyectoCV); [(video)](https://www.youtube.com/watch?v=SEwkZdSYvYc&feature=youtu.be); [(+info)](https://github.com/pecons/proyectoCV/blob/master/presentation/presentaci%C3%B3nFinal.pdf)


---
## Reconocimiento de actividades en video utilizando un descriptor de covarianza local <a name="proy9"></a>

**Autores: Oscar Mendoza**

<img src="https://github.com/oskkr123/Computer-vision/blob/master/ut.png" style="width:700px;">

**Objetivo: Proponer un descriptor de covarianza volumetrica local con soporte en trayecto**  

- Dataset: UT Interactions 6 acciones con 10 videos cada una
- Modelo:Bag of words, CNN/DNN, Imagen integral, Trayectorias densas de movimiento 

[(code)](https://github.com/oskkr123/Computer-vision); [(video)](https://drive.google.com/file/d/1lPnAo0DeTuJQXCtXsKZ60sxxRICiaGk-/view?usp=sharing); [(+info)](https://drive.google.com/file/d/1KC7eBL8DKOWWOhy3mAhz2kgMOntH3me0/view?usp=sharing)

---
## Cardiac Cycle Generation <a name="proy12"></a>

**Autores: Santiago Gomez**

<img src="https://github.com/Sangohe/CV-Cardiac_Cycle_Generation/blob/master/img/bannerCVProject.png" style="width:700px;">

**Objetivo: Generar secuencias de ciclos cardiacos cine-mri con técnicas de aprendizaje profundo**  

- Dataset: 401 ciclos cardiacos que estan conformados cada uno por 20 frames, 45 pacientes y 4 patologías.
- Modelo: Redes neuronales, Redes generativas adversarias convolucionales, redes generativas adversarias convolucionales de crecimiento progresivo

[(code)](https://github.com/Sangohe/CV-Cardiac_Cycle_Generation) [(+info)](https://github.com/Sangohe/CV-Cardiac_Cycle_Generation/blob/master/CV_project_slides.pdf)

---


## SamrtDamp <a name="proy11"></a>

**Autores: Jheyson Jaimes, Andres Hernandez, Luis Carlos Jimenez**

<img src="imgs/smartDump.jpeg" style="width:700px;">

**Objetivo:Buscar una forma eficiente para clasificar desechos por medio de imágenes para contribuir al adecuado manejo de desechos solidos.**  

- Dataset: 9000 imágenes sacadas de Imagenet relacionadas con el problema
- Modelo: CNN 

[(code)](https://github.com/jheysonjaimes/cv-proyect); [(video)](https://github.com/jheysonjaimes/cv-proyect/blob/master/SMART%20DUMP.mp4); [(+info)](imgs/DiapositivasSmartDump.pdf)
---



